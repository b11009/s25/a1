db.fruits.aggregate([
    {$match: {onSale: true}},
    {$unwind: "$origin"},
    {$group: {_id: "$onSale", totalFruitsOnSale: {$sum : "$stocks"}}}
]);

db.fruits.aggregate([
    {$match: 
        {stocks: {$gt:20}}
    },

    {
        $count: "Total Number of Fruits with stock more than 20"
    }
]);

db.fruits.aggregate([
    {$match: {onSale: true}},
    {$group: {_id: "$supplier_id",
        averagePriceOnSalePerSupplier: {$avg : {$multiply: ["$price","$stocks"]}}
    }}
]);

db.fruits.aggregate(
   [
     {
       $group:
         {
           _id: "$supplier_id",
           minPrice: { $min: "$price" }
         }
     }
   ]
);

db.fruits.aggregate(
   [
     {
       $group:
         {
           _id: "$supplier_id",
           maxPrice: { $max: "$price" }
         }
     }
   ]
);

